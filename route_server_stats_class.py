from influxdb import InfluxDBClient
from database import *
from large_communities import *
import json
import socket
import requests.packages.urllib3.util.connection as urllib3_cn
import requests
from datetime import datetime
from helpers import *

def allowed_gai_family():
    family = socket.AF_INET    # force IPv4
    return family
 
#FORCE IPV4 
urllib3_cn.allowed_gai_family = allowed_gai_family


def expand_community_tuple_to_rest(community_tuple):
    assert type(community_tuple) == tuple, "community must be represented by a tuple"
    community_size = len(community_tuple)
    community_in_slashes_string = "/"
    for community_index in range(0,community_size):
        # if it is not the last community index
        if (community_index!=community_size-1):
            community_in_slashes_string = community_in_slashes_string + str(community_tuple[community_index])+"/"
        else:
            community_in_slashes_string = community_in_slashes_string + str(community_tuple[community_index]) 
    return(community_in_slashes_string)


selected_communities = [IXP_LC_FILTERED_IRRDB_PREFIX_FILTERED,IXP_LC_FILTERED_IRRDB_ORIGIN_AS_FILTERED,IXP_LC_FILTERED_RPKI_INVALID,IXP_LC_INFO_RPKI_VALID,IXP_LC_INFO_RPKI_UNKNOWN,IXP_LC_INFO_IRRDB_VALID]
class Route_Server_stat:
	def __init__(self, rs):
		self.rs = rs
		self.peers = self.get_peers()
	
	def get_peers(self):
		try:
			birsdeye_peers_url = 'http://'+str(self.rs)+'/api/symbols/protocols'
			protocols = requests.get(url=birsdeye_peers_url)
			protocols_json = protocols.json()
			number_of_bgp_clients = len(protocols_json["symbols"])
			peers = protocols_json["symbols"]
		except:
			print("Data from birdseye failed to be retrieved")
			#tables_json["symbols"][i] -> i is birdseye arithemitc index for each clients table
		return(peers)
	def get_peers_number(self):
		peer_counter = 0
		for peer_index in range(0,len(self.peers)):
			peer = self.peers[peer_index] 
			if peer[:2]=="b_":
				peer_counter = peer_counter + 1
		return(peer_counter)
	def get_stat_from_rs_total(self,large_community):
		routes_counter = 0
		if large_community == None:
			requested_large_community_url = "http://"+str(self.rs)+"/api/routes/count/table/master"
		else:
			requested_large_community_url = "http://"+str(self.rs)+"/api/routes/count/lc-zwild/table/master"+expand_community_tuple_to_rest(large_community)
		requested_large_community_response = requests.get(url=requested_large_community_url)
		requested_large_community_response = requested_large_community_response.json()["count"]["routes"]
		routes_counter = requested_large_community_response
		#print(eval(str(large_community)),routes_counter)
		return(routes_counter)
  
	def get_stat_from_rs_calculated_per_peer(self,large_community):
		routes_counter = 0
		for peer_index in range(0,len(self.peers)):
			peer = self.peers[peer_index] 
			if peer[:2]=="b_":
				if large_community == None:
					requested_large_community_url = "http://"+str(self.rs)+"/api/routes/count/protocol/"+str(peer)
				else:
					requested_large_community_url = "http://"+str(self.rs)+"/api/routes/count/lc-zwild/protocol/"+str(peer)+expand_community_tuple_to_rest(large_community)
				requested_large_community_response = requests.get(url=requested_large_community_url)
				# if we do not get a proper response from Birdseye, we sleep and retry
				while (requested_large_community_response.status_code != 200):
					sleep(3)
					requested_large_community_response = requests.get(url=requested_large_community_url)
				requested_large_community_response = requested_large_community_response.json()["count"]["routes"]
				routes_counter = routes_counter + int(requested_large_community_response)
		#print(eval(str(large_community)),routes_counter)
		return(routes_counter)

    #return(routes_counter)
'''            
#Testing functionality
test_class = Route_Server_stat("rs1-vlan505-ipv4.gr-ix.gr")
test_class.get_peers()
test_class.get_stat_from_rs_total(IXP_LC_FILTERED_RPKI_INVALID)
test_class.get_stat_from_rs_calculated_per_peer(IXP_LC_FILTERED_RPKI_INVALID)
'''

#ROUTE SERVERS
#Route_Servers = ["rsx-vlan505-ipv4.gr-ix.gr","rs1-vlan505-ipv4.gr-ix.gr","rs0-vlan505-ipv4.gr-ix.gr"]
'''
for rs in Route_Servers:
	print(rs)
	rs_class = Route_Server_stat(rs)
	for community in selected_communities:
		print("Calculated per peer")
		rs_class.get_stat_from_rs_calculated_per_peer(community)
		print("Calculated in total")
		rs_class.get_stat_from_rs_total(community)

rs_class.get_stat_from_rs_calculated_per_peer(None)
rs_class.get_stat_from_rs_total(None)
'''

#CONNECT TO THE DATABASE
client = InfluxDBClient(influxdb_ip, infuxdb_port, influxdb_username, influxdb_passwd, influxdb_db)
try:
    version = client.ping()
    if (version):
        print("Successfully connected to InfluxDB: " + version)
except (Exception, e):
    send_email(sender,receivers,"InfluxDB Connection Issue","Python Influx Client could not connect to the database \n"+str(e))
    exit()

#selected_communities = [IXP_LC_FILTERED_IRRDB_PREFIX_FILTERED,IXP_LC_FILTERED_IRRDB_ORIGIN_AS_FILTERED,IXP_LC_FILTERED_RPKI_INVALID,IXP_LC_INFO_RPKI_VALID,IXP_LC_INFO_RPKI_UNKNOWN,IXP_LC_INFO_IRRDB_VALID]

#GET CURRENT TIME
current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
print(current_time)
# IPv4 + IPv6 Route Server stats, we handle each RS instance (IPv4, IPv6) as a different RS
Route_Servers = ["rs2-vlan505-ipv4.gr-ix.gr","rs1-vlan505-ipv4.gr-ix.gr","rs0-vlan505-ipv4.gr-ix.gr","rs0-vlan400-ipv4.thess.gr-ix.gr","rs1-vlan400-ipv4.thess.gr-ix.gr"]+["rs2-vlan505-ipv6.gr-ix.gr","rs1-vlan505-ipv6.gr-ix.gr","rs0-vlan505-ipv6.gr-ix.gr","rs0-vlan400-ipv6.thess.gr-ix.gr","rs1-vlan400-ipv6.thess.gr-ix.gr"]
for rs in Route_Servers:
    rs_class = Route_Server_stat(rs)
    input_to_influx = [{
        "measurement": "route_server_stats",
        "tags": {"rs" : str(rs)},
        "time": current_time,
        "fields": {
        "PEERS": rs_class.get_peers_number(),
        "ANNOUNCED_PREFIXES" : rs_class.get_stat_from_rs_calculated_per_peer(None),
        "IRRDB_VALIDS": rs_class.get_stat_from_rs_total(IXP_LC_INFO_IRRDB_VALID),
        "IRRDB_PREFIX_FILTERED": rs_class.get_stat_from_rs_calculated_per_peer(IXP_LC_FILTERED_IRRDB_PREFIX_FILTERED),
        "IRRDB_ORIGIN_AS_FILTERED": rs_class.get_stat_from_rs_calculated_per_peer(IXP_LC_FILTERED_IRRDB_ORIGIN_AS_FILTERED),
        "RPKI_VALIDS": rs_class.get_stat_from_rs_total(IXP_LC_INFO_RPKI_VALID),
        "RPKI_INVALIDS":rs_class.get_stat_from_rs_calculated_per_peer(IXP_LC_FILTERED_RPKI_INVALID),
        "RPKI_UNKNOWN": rs_class.get_stat_from_rs_total(IXP_LC_INFO_RPKI_UNKNOWN),
        "ACCEPTED_PREFIXES": rs_class.get_stat_from_rs_total(None) 
        }
        }]
    #print(input_to_influx)
    try:        
        client.write_points(input_to_influx)
    except (Exception, e):
        send_email(sender,receivers,"InfluxDB Write Issue - InfluxDB service requires restart","Python Influx Client could not write to the database \n"+str(e))


