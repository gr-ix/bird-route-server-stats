import smtplib
from email.mime.text import MIMEText
smtp_port = 25

sender = 'nic@gr-ix.gr'
receivers = ['mdimolianis@netmode.ntua.gr','dimolianis.marinos@gmail.com']

def send_email(sender,receivers,subject,text):
    msg = MIMEText(str(text))
    msg['Subject'] = subject
    msg['From'] = sender
    msg['To'] = ','.join(receivers)
    smtp_server = smtplib.SMTP('localhost', smtp_port)
    smtp_server.sendmail(sender, receivers, msg.as_string())

#send_email(sender,receivers,"Test","Test")
