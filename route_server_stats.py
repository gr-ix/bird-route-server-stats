from influxdb import InfluxDBClient
from database import *
from large_communities import *
import json
import socket
import requests.packages.urllib3.util.connection as urllib3_cn
import requests
from datetime import datetime

def allowed_gai_family():
    family = socket.AF_INET    # force IPv4
    return family
 
#FORCE IPV4 
urllib3_cn.allowed_gai_family = allowed_gai_family


def expand_community_tuple_to_rest(community_tuple):
    assert type(community_tuple) == tuple, "community must be represented by a tuple"
    community_size = len(community_tuple)
    community_in_slashes_string = "/"
    for community_index in range(0,community_size):
        # if it is not the last community index
        if (community_index!=community_size-1):
            community_in_slashes_string = community_in_slashes_string + str(community_tuple[community_index])+"/"
        else:
            community_in_slashes_string = community_in_slashes_string + str(community_tuple[community_index]) 
    return(community_in_slashes_string)



#print(client)

#ROUTE SERVERS
Route_Servers = ["rsx-vlan505-ipv4.gr-ix.gr","rs1-vlan505-ipv4.gr-ix.gr","rs0-vlan505-ipv4.gr-ix.gr","rs0-vlan400-ipv4.thess.gr-ix.gr","rs1-vlan400-ipv4.thess.gr-ix.gr"]

#REQUESTED PARAMETERS PER RS
number_of_peers_per_rs = {}
number_of_announced_prefixes_per_rs = {}
number_of_irrdb_discards_per_rs = {}
number_of_irrdb_valids_per_rs = {}
number_of_rpki_invalids_per_rs = {}
number_of_irrdb_origin_as_filtered_per_rs = {}
number_of_rpki_valids_per_rs = {}
number_of_rpki_unknown_per_rs = {}
number_of_accepted_prefixes_per_rs = {}

#GET ROUTE SERVER
for RS in Route_Servers:
    # initialize temporary counting variables for each RS
    announced_prefixes = 0
    irrdb_discards = 0
    irrdb_valids = 0
    irrdb_origin_as_filtered = 0
    rpki_valids = 0
    rpki_invalids = 0
    rpki_unknown = 0
    number_of_peers_counter = 0
    #GET ROUTE SERVER PROTOCOLS
    url = 'http://'+str(RS)+'/api/symbols/protocols'
    protocols = requests.get(url=url)
    protocols_json = protocols.json()
    #tables_json["symbols"][i] -> i is birdseye arithemitc index for each clients table
    number_of_bgp_clients = len(protocols_json["symbols"])
    
    number_of_peers_per_rs[RS] = number_of_bgp_clients
    for protocol_client_index in range(0,number_of_bgp_clients):
        peer = protocols_json["symbols"][protocol_client_index]
        #protocol_name corresponds to the peer
        
        # Get only protocols that start with b_, as these correspond to peers
        if peer[:2]=="b_":
            # Correct peer that starts with b_
            # Retrieve announced prefixes per peer
            number_of_peers_counter = number_of_peers_counter + 1
            announced_prefixes_url = "http://"+str(RS)+"/api/routes/count/protocol/"+str(peer)
            announced_prefixes_response = requests.get(url=announced_prefixes_url)            
            announced_prefixes_response = announced_prefixes_response.json()["count"]["routes"]
            announced_prefixes = announced_prefixes + int(announced_prefixes_response)
            # Retrieve irrdb_discards per peer
            irrdb_discards_url = "http://"+str(RS)+"/api/routes/count/lc-zwild/protocol/"+str(peer)+expand_community_tuple_to_rest(IXP_LC_FILTERED_IRRDB_PREFIX_FILTERED)
            irrdb_discards_response = requests.get(url=irrdb_discards_url)            
            irrdb_discards_response = irrdb_discards_response.json()["count"]["routes"]
            irrdb_discards = irrdb_discards + int(irrdb_discards_response)
            
            # Retrieve origin_as_filtered per peer
            irrdb_origin_as_filtered_url = "http://"+str(RS)+"/api/routes/count/lc-zwild/protocol/"+str(peer)+expand_community_tuple_to_rest(IXP_LC_FILTERED_IRRDB_ORIGIN_AS_FILTERED)
            irrdb_origin_as_filtered_response = requests.get(url=irrdb_origin_as_filtered_url)            
            irrdb_origin_as_filtered_response = irrdb_origin_as_filtered_response.json()["count"]["routes"]
            irrdb_origin_as_filtered = irrdb_origin_as_filtered + int(irrdb_origin_as_filtered_response)    
            # Retrieve rpki_invalids per peer
            rpki_invalids_url = "http://"+str(RS)+"/api/routes/count/lc-zwild/protocol/"+str(peer)+expand_community_tuple_to_rest(IXP_LC_FILTERED_RPKI_INVALID)
            rpki_invalids_response = requests.get(url=rpki_invalids_url)            
            rpki_invalids_response = rpki_invalids_response.json()["count"]["routes"]
            rpki_invalids = rpki_invalids + int(rpki_invalids_response)

            
      
    # These are statistics for routes that are in the master table of the RS, so we do not need to calculate them per peer but only once       
    rpki_valids_url = "http://"+str(RS)+"/api/routes/count/lc-zwild/table/master"+expand_community_tuple_to_rest(IXP_LC_INFO_RPKI_VALID)
    rpki_valids_response = requests.get(url=rpki_valids_url)            
    rpki_valids_response = rpki_valids_response.json()["count"]["routes"]
    rpki_valids =int(rpki_valids_response)
    
    # Attention next to master we do not need a backslash since it is generated by the expand_community_tuple_to_rest
    
    rpki_unknown_url = "http://"+str(RS)+"/api/routes/count/lc-zwild/table/master"+expand_community_tuple_to_rest(IXP_LC_INFO_RPKI_UNKNOWN)
    rpki_unknown_response = requests.get(url=rpki_unknown_url)            
    rpki_unknown_response = rpki_unknown_response.json()["count"]["routes"]
    rpki_unknown = int(rpki_unknown_response)
    
    irrdb_valids_url = "http://"+str(RS)+"/api/routes/count/lc-zwild/table/master"+expand_community_tuple_to_rest(IXP_LC_INFO_IRRDB_VALID)
    irrdb_valids_response = requests.get(url=irrdb_valids_url)            
    irrdb_valids_response = irrdb_valids_response.json()["count"]["routes"]
    irrdb_valids =int(irrdb_valids_response)
    
    accepted_prefixes_url = announced_prefixes_url = "http://"+str(RS)+"/api/routes/count/table/master"
    accepted_prefixes_response = requests.get(url=accepted_prefixes_url)            
    accepted_prefixes_response = accepted_prefixes_response.json()["count"]["routes"]
    accepted_prefixes = accepted_prefixes_response
          
            
    number_of_peers_per_rs[RS] = number_of_peers_counter       
    number_of_announced_prefixes_per_rs[RS] = announced_prefixes
    number_of_irrdb_discards_per_rs[RS] = irrdb_discards
    number_of_irrdb_origin_as_filtered_per_rs[RS] = irrdb_origin_as_filtered
    number_of_rpki_invalids_per_rs[RS] = rpki_invalids
    number_of_rpki_valids_per_rs[RS] = rpki_valids
    number_of_irrdb_valids_per_rs[RS] = irrdb_valids
    number_of_rpki_unknown_per_rs[RS] = rpki_unknown
    number_of_accepted_prefixes_per_rs[RS] = accepted_prefixes
    
    #print(number_of_announced_prefixes_per_rs)
    #print(number_of_irrdb_discards_per_rs)
    #print(number_of_rpki_invalids_per_rs)
    #print(number_of_rpki_valids_per_rs)
    #print(number_of_rpki_unknown_per_rs)
    
#CONNECT TO THE DATABASE
client = InfluxDBClient(influxdb_ip, infuxdb_port, influxdb_username, influxdb_passwd, influxdb_db)
version = client.ping()
print("Successfully connected to InfluxDB: " + version)

#GET CURRENT TIME
current_time = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')
print(current_time)
for RS in Route_Servers:
    input_to_influx = [{
        "measurement": "route_server_stats",
        "tags": {"rs" : str(RS)},
        "time": current_time,
        "fields": {
        "PEERS": number_of_peers_per_rs[RS],
        "ANNOUNCED_PREFIXES" : number_of_announced_prefixes_per_rs[RS],
        "IRRDB_VALIDS": number_of_irrdb_valids_per_rs[RS],
        "IRRDB_PREFIX_FILTERED": number_of_irrdb_discards_per_rs[RS],
        "IRRDB_ORIGIN_AS_FILTERED": number_of_irrdb_origin_as_filtered_per_rs[RS],
        "RPKI_VALIDS": number_of_rpki_valids_per_rs[RS],
        "RPKI_INVALIDS": number_of_rpki_invalids_per_rs[RS],
        "RPKI_UNKNOWN": number_of_rpki_unknown_per_rs[RS],
        "ACCEPTED_PREFIXES": number_of_accepted_prefixes_per_rs[RS]
        }
        }]
    #print(input_to_influx)        
    client.write_points(input_to_influx)
